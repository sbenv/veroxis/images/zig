#!/usr/bin/env sh

set -eu

apk add --no-cache tar xz curl jq

export ZIG_ARCHITECTURE=$(uname -m)
export ZIG_VERSION=$(./parse_zig_version.sh)

set -x

curl -s --fail -o "${ZIG_VERSION}.tar.xz" "https://ziglang.org/download/${ZIG_VERSION}/zig-linux-${ZIG_ARCHITECTURE}-${ZIG_VERSION}.tar.xz"
tar -xvf "${ZIG_VERSION}.tar.xz"

mv "zig-linux-${ZIG_ARCHITECTURE}-${ZIG_VERSION}" "zig"
mv "zig/zig" "/usr/bin/zig"
mv "zig/lib" "/usr/lib/zig"
mkdir -p "/usr/share/licenses/zig"
mv "zig/LICENSE" "/usr/share/licenses/zig/LICENSE"
