#!/usr/bin/env sh

set -eu

apk add --no-cache tar xz curl jq

export ZIG_ARCHITECTURE=$(uname -m)
export ZIG_VERSION=$(curl -s https://ziglang.org/download/index.json | jq -e ".[\"master\"][\"${ZIG_ARCHITECTURE}-linux\"][\"tarball\"]" | sed 's/"//g;s/https:\/\/ziglang.org\/builds\///g;s/.tar.xz//g')

set -x

curl -s --fail -o "${ZIG_VERSION}.tar.xz" "https://ziglang.org/builds/${ZIG_VERSION}.tar.xz"
tar -xvf "${ZIG_VERSION}.tar.xz"

mv "${ZIG_VERSION}" "zig"
mv "zig/zig" "/usr/bin/zig"
mv "zig/lib" "/usr/lib/zig"
mkdir -p "/usr/share/licenses/zig"
mv "zig/LICENSE" "/usr/share/licenses/zig/LICENSE"
