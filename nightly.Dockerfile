FROM registry.gitlab.com/sbenv/veroxis/images/alpine:3.20.2 as zig-source
COPY "install_zig_nightly.sh" "install_zig.sh"
RUN apk add --no-cache "tar" "xz" "curl" "jq"
RUN sh "install_zig.sh"

FROM registry.gitlab.com/sbenv/veroxis/images/alpine:3.20.2
COPY --from=zig-source "/usr/bin/zig" "/usr/bin/zig"
COPY --from=zig-source "/usr/lib/zig" "/usr/lib/zig"
COPY --from=zig-source "/usr/share/licenses/zig/LICENSE" "/usr/share/licenses/zig/LICENSE"
